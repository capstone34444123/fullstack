import { useContext } from "react"
import UserContext from "../UserContext";
import { NavLink } from "react-router-dom";
import '../App.css';
import './Components.css';



import { Container, Nav, Navbar, NavDropdown } from "react-bootstrap";


export default function AppNavbar(){
      const { user } = useContext(UserContext);
     const userFirstName = `${localStorage.getItem("firstName")}`;
     console.log(user);
   
    return(
        
        <Navbar expand="lg"  className="shadow bg-white sticky-top m-0 px-2">
        <Container fluid>
            <Navbar.Brand as={ NavLink } to="/" end className="font-weight-bold" id="home">
            
            CA
        </Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="ms-auto ">
            
            <Nav.Link as={ NavLink } to="/" end id="home">Home</Nav.Link>
            <Nav.Link as={ NavLink } to="/products" end id="home">Products</Nav.Link>

            {

                (user.token !== null)
                ?
                <>
                 <Nav.Link as={ NavLink } to="/logout" end id="home">Logout</Nav.Link>
                 
                </>
                :
                <>
                
                <Nav.Link as={ NavLink } to="/login" end id="home">Login</Nav.Link>
                <Nav.Link as={ NavLink } to="/register" end id="home">Register</Nav.Link>

                
                </>



            }
            {
                (user.isAdmin)
                ?
                <Nav.Link as={ NavLink } to="/dashboard" end id="home">Dashboard</Nav.Link>
                :
                <>
                </>
            }
            


            
            

            
        </Nav>

            </Navbar.Collapse>
        </Container>
        </Navbar>
        
    );
}

