import { Row, Col, Button, Image } from "react-bootstrap";
import { Link } from "react-router-dom"
import bg2 from "./image/bg2.png";

export default function Banner({bannertop}){
	console.log(bannertop);

	const {title, content, destination, label} = bannertop;

	return(
		
		
		<div className="vh-100 mb-5" id="bghome">
				
			<Row>
				<Col className="p-5 text-light">
					<h1 id="banner">{title}</h1>
					<h5 className="pt-2 pb-4" id="con">{content}</h5>
					<Button id="but" as={Link} to={destination} className="btn-danger text-bold" variant="">{label}</Button>
				</Col>
			</Row>
		</div>
		)
}