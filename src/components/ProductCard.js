import { Card, Button, Col, Row } from 'react-bootstrap';
import { useState } from "react"
import {Link} from "react-router-dom"
import acer from "./image/acer.jfif"



export default function ProductCard({productProp}){

     console.log(productProp.name);
  
    console.log(typeof productProp);

    const { _id, name, description, price, stocks} = productProp;


    return(
        <div id="card">        
        <Col className="my-3" xs={12} md={6} lg={4}>               
        <Card>
            <Card.Img className='img-fluid w-100 product-img-fit'
                src={acer}
            />
            <Card.Body >
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>{price}</Card.Text>
                <Card.Subtitle>Stocks:</Card.Subtitle>
                <Card.Text>{stocks}</Card.Text>
                <Link className="btn btn-success" to={`/productView/${_id}`}>Details</Link>
            </Card.Body>
        </Card>
    </Col>
</div>
        
        
        

                
        )
}