import { Row, Col, Card, Button, Image } from "react-bootstrap";
import acer from "./image/acer.jfif";
import samsung from "./image/samsung.jfif";
import lenovo from "./image/lenovo.jfif";


export default function Hightlights(){
	return(
			<div>
			<Row className="my-3">
            <Col className="my-2" xs={12} md={4}>
                <Card className="cardHighlight" id="lights">
                    <Card.Body>
                    <Card.Header>
                    <Image width="290" height="200" src={acer}/>
                        <Card.Title>
                            <h2 className="pt-2 d-flex align-items-center justify-content-center">Acer</h2>
                        </Card.Title>
                    </Card.Header>
                        <Card.Text>
                            Acer has a great reputation and is one of the leading laptop brands.
    					     {/*<Button to="/products">View</Button>							*/}
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col className="my-2" xs={12} md={4}>
                <Card className="cardHighlight" id="lights">
                    <Card.Body>
                    <Card.Header>
                    <Image width="290" height="200" src={samsung}/>
                        <Card.Title>
                            <h2 className="pt-2 d-flex align-items-center justify-content-center">Samsung NoteBook</h2>
                        </Card.Title>
                        </Card.Header>
                        <Card.Text>
                            Acer has a great reputation and is one of the leading laptop brands.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col className="my-2" xs={12} md={4}>
                <Card className="cardHighlight" id="lights">
                    <Card.Body>
                        <Card.Header>
                        <Image width="290" height="200" src={lenovo}/>
                        <Card.Title>
                            <h2 className="pt-2 d-flex align-items-center justify-content-center">Lenovo</h2>
                        </Card.Title>
                        </Card.Header>
                        <Card.Text>
                            Acer has a great reputation and is one of the leading laptop brands.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
        </Row>			
			
			</div>

		)
}




{/*<Row className="mt-3">
				<Col xs={12} md={4}>
					 <Card style={{ width: '18rem' }}>
    					  <Card.Img variant="top" src="bg2.png"/>
    					  <Card.Body>
    					    <Card.Title>Acer</Card.Title>
    					    <Card.Text>
    					       Acer has a great reputation and is one of the leading laptop brands.
    					     <Button to="/products">View</Button>							
   					     	</Card.Text>
    					    
    					  </Card.Body>
    				</Card>
				</Col>

				<Col xs={12} md={4}>
					 <Card style={{ width: '18rem' }}>
    					  <Card.Img variant="top" src="bg2.png"/>
    					  <Card.Body>
    					    <Card.Title>Samsung</Card.Title>
    					    <Card.Text>
    					       Acer has a great reputation and is one of the leading laptop brands.
   					     	</Card.Text>
    					    
    					  </Card.Body>
    				</Card>
				</Col>

				<Col xs={12} md={4}>
					 <Card style={{ width: '18rem' }}>
    					  <Card.Img variant="top" src="bg2.png"/>
    					  <Card.Body>
    					    <Card.Title>Lenovo</Card.Title>
    					    <Card.Text>
    					       Acer has a great reputation and is one of the leading laptop brands.
   					     	</Card.Text>
    					    
    					  </Card.Body>
    				</Card>
				</Col>

			
			</Row>*/}