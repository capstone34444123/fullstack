import { Row, Col, Card, Image } from "react-bootstrap";
import macbook from "./image/macbook.jpg";
import Dell from "./image/dell.jfif";
import hp from "./image/hp.jfif";

export default function HotDeals(){

    return(
        <Row className="my-3">
            <Col className="my-2" xs={12} md={4}>
                <Card className="cardHighlight" id="hot">
                    <Card.Body>
                    <Image width="320" height="200" src={macbook}/>
                    <Card.Header>                   
                        <Card.Title>
                            <h2 className="pt-2 d-flex align-items-center justify-content-center">Macbook</h2>
                        </Card.Title>
                    </Card.Header>
                        <Card.Text className="pt-3">
                            Apple laptops come with macOS, Apple's operating system, which is a highly stable and user-friendly platform, built especially for Apple's computers. Many people find it easier to use macOS compared to Windows.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col className="my-2" xs={12} md={4}>
                <Card className="cardHighlight" id="hot">
                    <Card.Body>
                    <Image width="320" height="200" src={Dell}/>
                    <Card.Header>
                        <Card.Title>
                            <h2 className="pt-2 d-flex align-items-center justify-content-center">Dell</h2>
                        </Card.Title>
                        </Card.Header>
                        <Card.Text className="pt-3">
                            DELL COMPUTER SYSTEM means a desktop, laptop, server, or workstation computer sold by Dell under its own brand or trade name, regardless of the source of its supply, production, manufacture, assembly, design, or origin.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col className="my-2" xs={12} md={4}>
                <Card className="cardHighlight" id="hot">
                    <Card.Body>
                    <Image width="320" height="200" src={hp}/>
                        <Card.Header>
                        <Card.Title>
                            <h2 className="pt-2 d-flex align-items-center justify-content-center">Hp</h2>
                        </Card.Title>
                        </Card.Header>
                        <Card.Text className="pt-3">
                            Hewlett-Packard Company, American manufacturer of software and computer services and a major brand in the history of computers and computer-related products. The company split in 2015 into two companies: HP Inc. and Hewlett Packard Enterprise.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    )
}