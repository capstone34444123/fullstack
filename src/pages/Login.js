import { Form, Button, Image, a } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from "../UserContext"
import { Navigate } from "react-router-dom";
import Swal from "sweetalert2"
import "./Pages.css";
import Facebook from "./images/Facebook.png";
import Instagram from "./images/Instagram.png";
import YouTube from "./images/YouTube.png";







export default function Login(){

	const { user, setUser} = useContext(UserContext);

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [isActive, setIsActive] = useState(true);

	
	
	

	useEffect(() => {
		if(email !== '' && password !== ''){
			setIsActive(true);
		}else{
			setIsActive(false);
		}
	}, [email, password]);

	function authenticate(e) {
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: "POST",
			headers: {
				"Content-type" : "application/json"
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		}).then(res => res.json())
    .then(data => {
        console.log(data.accessToken);
        if(data.access !== "undefined"){
            localStorage.setItem("token", data.access);
            retrieveUserDetails(data.access);

				Swal.fire({
					icon: "success",
					title: "Login Successfully!",
					text: "Welcome to Computer Store",
				})
			}else{
				Swal.fire({
					icon: "error",
					title: "Failed!",
					text: "Try again",
				})
			}
		})

		setEmail("");
		setPassword("");
		// alert(`${email} Welcome back!`);
	}


		const retrieveUserDetails = (token) => {
			fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
				method: "POST",
				headers: {
					Authorization: `Bearer ${token}`
				}
			}).then(res => res.json())
			  .then(data => {
				setUser({
					id: localStorage.setItem("id", data._id),
                firstName: localStorage.setItem("firstName", data.firstName),
                lastName: localStorage.setItem("lastName", data.lastName),
                email: localStorage.setItem("email", data.email),
                isAdmin: localStorage.setItem("isAdmin", data.isAdmin)
				})
			})
		}


	return(

		 (localStorage.getItem("id") !== null) ? //(user.token !== null)
            <Navigate to="/dashboard"/>
        :
        <>
        	<div id="loginwhole">
            <div className="container" id="login">
            
            <div className="row">
            <div className = "col-md-6" id="visit">

            	<h1>Visit our social media</h1>
			
            	<div className="pt-4">
            <a href="https://www.facebook.com/"><img width="80" height="80" src={Facebook} alt="" id="Facebook" /></a>
            <a href="https://www.instagram.com/"><img width="80" height="75" src={Instagram} alt="" id="Instagram" /></a>
            <a href="https://www.youtube.com/"><img width="80" height="80" src={YouTube} alt=""/></a>
            </div>


            </div>

            <div className="col-md-6 p-5">
            <Form onSubmit={(e) => authenticate(e)} className="my-5">
            <h1 className="d-flex flex-column align-items-center justify-content-center p-3 my-3">Login Now!</h1>
            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control
                    type="email" 
                    placeholder="Enter email" 
                    required
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password" 
                    required
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    required
                />
            </Form.Group>
            <div className="d-flex align-items-center justify-content-center" id="loginButton">
            { isActive ? 
                <Button variant="success" type="submit" id="submitBtn" className="mt-3">
                    Login
                </Button>
                : 
                <Button variant="danger" type="submit" id="submitBtn" className="mt-3" disabled>
                    Login
                </Button>
            }
            </div>
        </Form>
        	</div>
          </div>
        </div>
       </div>
        </>

		)
}