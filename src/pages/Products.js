import productData from "../data/product"
import UserContext from "../UserContext";
import Banner from "../components/Banner";
import ProductCard from "../components/ProductCard"
import { useEffect, useState, useContext} from "react"
import { Navigate } from "react-router-dom";





export default function Products(){


	// 	console.log(productData);
	// console.log(productData[0]);

	const data = {
		title: "Computer Online Store",
		content: "The most reliable Computer and laptops components",
		destination: "/products",
		label: "Shop Now!"
		
	}


   const { user } = useContext(UserContext);
   
   const [AllProducts, setAllProducts] = useState([]);

 useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/active/all`)
		.then(res => res.json())
		.then(data => {
			console.log(data);


			setAllProducts(data.map(product => {
				return (
					<ProductCard key={product._id} productProp={product}/>
					)
			}))

		})
	}, [])



	return(

		(user.isAdmin)
		?
		<Navigate to="/dashboard" />
		:
		<>
		<div>
		<h1>Products</h1>
		{AllProducts}

		</div>
		
		</>
		)
}