const express = require("express");
const router = express.Router();
const userController = require("../controllers/user.js");
const auth = require("../auth.js")



// user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

/*router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});*/


router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController =>
		res.send(resultFromController));
});

// login user
router.post("/login", (req, res) => {
	userController.userLogin(req.body).then(resultFromController => res.send(resultFromController))
});

// create orders

/*router.post("/order", (req, res) => {
	let data = {
		product: req.body,
		userId : auth.decode(req.headers.authorization).id,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}
	userController.orderUser(data).then(resultFromController => res.send(resultFromController));
});*/





// create admin account
/*router.put("/:admin", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	userController.createAdmin(req.params, req.body, isAdmin).then(resultFromController => res.send(resultFromController));
});*/

router.put("/:userId/admin", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	userController.activate(req.params, req.body, isAdmin).then(resultFromController => res.send(resultFromController));
});



// user details

/*router.post("/details", auth.verify, (req, res) =>{
	const userDetails = auth.decode(req.headers.authorization);
	userController.details(userDetails.id).then(resultFromController => res.send(resultFromController));
})*/

// router.post("/details", auth.verify, (req, res) =>{
// 	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
// 	userController.profile(req.body, isAdmin).then(resultFromController => res.send(resultFromController));
// })

router.get("/details", auth.verify, (req, res) => {

    // Uses the "decode" method defined in the "auth.js" file to retrieve the user information from the token passing the "token" from the request header as an argument
    const userData = auth.decode(req.headers.authorization).id;

    // Provides the user's ID for the getProfile controller method
    userController.getProfile(userData).then(resultFromController => res.send(resultFromController));

});





// all user register
router.get("/all", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	userController.all(req.body, isAdmin).then(resultFromController => res.send(resultFromController));
});


// delete user

router.delete("/:userId/delete", (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	userController.deleteUser(req.params, isAdmin).then(resultFromController => res.send(resultFromController));
});




// allows us to export the "router" object that will be access in our index.js
module.exports = router;