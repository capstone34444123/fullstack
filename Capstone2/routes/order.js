const express = require("express");
const router = express.Router();
const orderController = require("../controllers/orderController")
const auth = require("../auth");


// Add order
router.post("/:productId/addToOrder", auth.verify, (req, res) => {
  const user = auth.decode(req.headers.authorization);

  orderController.addToOrder(user, req.params, req.body).then((resultFromController) => res.send(resultFromController));
});


// checkout order

router.get("/", auth.verify, (req, res) => {
  const userId = req.params.userId
  orderController.getOrdersForAllUser(userId).then(result => res.send(result))
})



// specific order
router.get("/myOrders", auth.verify, (req, res) => {
  let isAdmin = auth.decode(req.headers.authorization).isAdmin;
  const userId = auth.decode(req.headers.authorization).id
  orderController.getOrdersForUser(userId, isAdmin).then(result => res.send(result))
})



module.exports = router;