const User = require("../models/Users.js");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Product = require("../models/Products.js");






// register
module.exports.registerUser = (reqBody) =>{
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo : reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
	});

	return newUser.save().then((user, error) =>{
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
};


// checkemail

module.exports.checkEmailExists = (reqBody) => {

	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0) {
			return true
		}else {
			return false
		}
	})

}


/*module.exports.registerUser = (reqBody) =>{
		if(reqBody.email === " " || reqBody.mobileNo === " " || reqBody.password === " "){
			return false
		}else {
			let newUser = new User({
		email: reqBody.email,
		mobileNo : reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
	});

	return newUser.save().then((user, error) =>{
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
		}
	
	
}
*/


// login

module.exports.userLogin = (reqBody) =>{

	return User.findOne({email: reqBody.email}).then(login => {
		console.log(login)
		if(login == null){
			return false;
		}else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, login.password)
			if(isPasswordCorrect){
				return {access: auth.createAccessToken(login)}
			}else{
				return false;
			}
		}
	})
	
};



// create orders

/*module.exports.orderUser = async (data) =>{
	if(data.isAdmin){
		return "ADMIN unable to order the product."
	}else{
		let isUserUpdate = await User.findById(data.userId).then(user => {
			let isOrderedUpdate = Product.findById(data.product.productId).then(product => {
				user.orderedProduct.push(
				{
					 product: [{
                        productId: data.product.productId,
                        name: product.name,
                        quantity: data.product.quantity
                    }],
                    totalAmount: product.price * data.product.quantity
				});
				return user.save().then((user, error) => {
					if(error){
						return false;
					}else{
						return true;

					}
				})
			})
			if(isOrderedUpdate){
				return Promise.resolve(true);
			}else{
				return Promise.resolve(false);
			}
		})
	}
}*/






// create admin
/*module.exports.createAdmin = (reqParams, reqBody, isAdmin) => {
	if(isAdmin){
		let updatedUser = {
			isAdmin: reqBody.isAdmin
	
		}
		return User.findByIdAndUpdate(reqParams.userId, updatedUser).then((update, error) => {
			if(error){
				return false;
			}
			else{
				return "Now activation to admin account has been successful."
			}

		})
	}


	let message = Promise.resolve("No access to update user.");
	return message.then((value) => {
		return value;
	})
	
};*/

module.exports.activate = (reqParams, reqBody, isAdmin) => {
	console.log(isAdmin);
	if(isAdmin){
		let updatedUser = {
			isAdmin: reqBody.isAdmin
	
		}
		return User.findByIdAndUpdate(reqParams.userId, updatedUser).then((update, error) => {
			if(error){
				return false;
			}
			else{
				return "Now activation to admin account has been successful."
			}

		})
	}


	let message = Promise.resolve("No access to update user.");
	return message.then((value) => {
		return value;
	})
	
};





// user details
/*module.exports.details = (userDetails) => {
	return User.findById(userDetails).then((detail, error) => {
		if(error){
			console.log(error);
			return false;
		}else{
			detail.password = ""
			return detail;
		}
	})
}*/

// module.exports.profile = (reqBody, isAdmin) => {
// 	if(isAdmin){
// 		let userProfiles = {
// 			userId : reqBody.userId
// 		}
// 		return User.findById(reqBody).then(result => {
// 			return result;
// 		})
		
// 	}
// 	else{
// 		return Promise.resolve("You don't have an acces to view the details.")
// 	}

// }

module.exports.getProfile = (reqBody) => {

    return User.findById(reqBody).then(result => {
        if (result == null) {
            return false;
        }else {
            result.password = ""

            return result;
        }
    })

};



// all user register
module.exports.all = (reqBody, isAdmin) => {
	if(isAdmin){
		return User.find({isAdmin: false}).then(all =>{
			return all;
		})
	}
	let message = Promise.resolve("you don't have an acces to view all register account");
	return message.then((value) => {
		return value;
	})
}


// delete user
module.exports.deleteUser = ( reqParams, isAdmin) => {
	

	if(isAdmin){
		return User.findByIdAndRemove(reqParams.userId).then(erase => {
			return erase
		})
	}else{
		return false;
	}
};



	





